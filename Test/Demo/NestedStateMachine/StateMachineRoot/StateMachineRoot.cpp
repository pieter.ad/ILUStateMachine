/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "ToState.h"

#include "CData.h"
#include "CState1.h"
#include "CStateMachineChild.h"
#include "StateConstrDestr.h"
#include "StateMachineRoot.h"

using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineChild;

//StateMachineRoot common implementation
namespace DemoNestedStateMachineRoot {
   SPStateMachine CreateStateMachine(void)
   {
      //create & return state machine
      return SPStateMachine(
         new TStateMachineWithData<Internal::CData>(
            "state-machine-root",                                                //< name used for logging
            Internal::SPData(new Internal::CData("state machine root message")), //< single state machine data instance
            ToState<Internal::CState1, Internal::CData>()                        //< used to create the initial state
         )
      );
   }

   void RootCheckStatus(
                        const bool   bState1Constructed,
                        const bool   bState1Destructed,
                        const bool   bState1EventHandlerCalled,
                        const bool   bState2Constructed,
                        const bool   bState2Destructed,
                        const bool   bState2EventHandlerCalled,
                        const bool   bState3Constructed,
                        const bool   bState3Destructed,
                        const bool   bState3EventHandlerCalled,
                        const int    iLine
                        )
   {
      return Internal::CheckStatus(
                                   bState1Constructed,
                                   bState1Destructed,
                                   bState1EventHandlerCalled,
                                   bState2Constructed,
                                   bState2Destructed,
                                   bState2EventHandlerCalled,
                                   bState3Constructed,
                                   bState3Destructed,
                                   bState3EventHandlerCalled,
                                   iLine
                                   );
   }

   void ChildCheckStatus(
                         const int  iNbrInstances,
                         const bool bState1Constructed,
                         const bool bState1Destructed,
                         const bool bState1EventHandlerCalled,
                         const bool bState2Constructed,
                         const bool bState2Destructed,
                         const bool bState2EventHandlerCalled,
                         const bool bState3Constructed,
                         const bool bState3Destructed,
                         const bool bState3EventHandlerCalled,
                         const int  iLine
                         )
   {
      return CStateMachineChild::CheckStatus(
                                             iNbrInstances,
                                             bState1Constructed,
                                             bState1Destructed,
                                             bState1EventHandlerCalled,
                                             bState2Constructed,
                                             bState2Destructed,
                                             bState2EventHandlerCalled,
                                             bState3Constructed,
                                             bState3Destructed,
                                             bState3EventHandlerCalled,
                                             iLine
                                             );
   }
};

