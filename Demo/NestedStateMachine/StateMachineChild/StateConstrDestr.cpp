/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CCreateStateFinished.h"
#include "CStateChangeException.h"
#include "ToState.h"

#include "EEventChild.h"

#include "CState1.h"
#include "CState2.h"
#include "CState3.h"

using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;

namespace DemoNestedStateMachineChild {
   namespace Internal {
      /****************************************************************************************
       ** 
       ** CState1
       **
       ***************************************************************************************/
      CState1::CState1(SPData spData)
         : CStateEvtId("child-state-1")
         , m_spData(spData)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
         EventRegister(HANDLER(CEventChildData, CState1, HandlerEvt1), ToState<CState2, CData>(), EEventChild1);
      }
      
      CState1::~CState1(void)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
      }

      /****************************************************************************************
       ** 
       ** CState2
       **
       ***************************************************************************************/
      CState2::CState2(SPData spData)
         : CStateEvtId("child-state-2")
         , m_spData(spData)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
         EventRegister(HANDLER(CEventChildData, CState2, HandlerEvt2), ToState<CState3, CData>(), EEventChild2);
      }
      
      CState2::~CState2(void)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
      }

      /****************************************************************************************
       ** 
       ** CState3
       **
       ***************************************************************************************/
      CState3::CState3(SPData spData)
         : CStateEvtId("child-state-3")
         , m_spData(spData)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
         EventRegister(HANDLER(CEventChildData, CState3, HandlerEvt3), FINISHED_STATE_CHANGE, EEventChild3);
      }
      
      CState3::~CState3(void)
      {
         LogInfo("%s '%s'\n", GetName().c_str(), __FUNCTION__);
      }
   };      
};
   
