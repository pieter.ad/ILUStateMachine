/** @file
 ** @brief The CStateEvtId definition (of none-template functions).
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CStateEvtId.h"
#include "CTypeInfo.h"

namespace ILULibStateMachine {
   /** Constructor.
    **/
   CStateEvtId::CStateEvtId(
      const char* const szName //< State name, used for logging only.
      )
      : CState(szName)
   {
   }

   /** Destructor.
    **/
   CStateEvtId::~CStateEvtId(void)
   {
   };

   std::string CStateEvtId::ComposeTypeHandlerIdString(
      const std::string&    strEventType, //< first part of the ID string, coming fromt the event ID type
      const std::type_info& typeinfo      //< second part of the ID string, coming from the event data type
      )
   {
      return(
             strEventType +
             std::string("_") +
             CTypeInfo::Demangle(typeinfo)
             );
   }
} // namespace ILULibStateMachine

