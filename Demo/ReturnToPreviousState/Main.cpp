/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CStateReturnToPrevious;

/****************************************************************************************
 ** 
 ** ToState templates taking a SPCreateState as a parameter.
 ** so they can be used when regestering state transitions.
 ***************************************************************************************/
namespace {
   namespace Ret2Prev {
      template<class CStateType> CState* ToStateInstanceNoData(SPCreateState spCreateNextState)
      {
         return new CStateType(spCreateNextState);
      }
   };
   
   template<class CStateType> SPCreateState ToState(SPCreateState spCreateNextState)
   {
      return SPCreateState(
         new CCreateStateNoData(
            TYPESEL::bind(
               &Ret2Prev::ToStateInstanceNoData<CStateType>, 
               spCreateNextState
               )
         )
      );
   }
};

/****************************************************************************************
 ** 
 ** First state
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1),                    //< the event handler
                    ToState<CStateReturnToPrevious>( ToState<CState1>() ), //< the state transition to CStateReturnToPrevious, specifiying also the state transition for CStateReturnToPrevious (reverting back to this state) 
                    1                                                      //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
   }
};

/****************************************************************************************
 ** 
 ** State returning to previous state
 **
 ***************************************************************************************/
/** Class definition of state-return-to-previous
 **/
class CStateReturnToPrevious : public CStateEvtId {
public:
   CStateReturnToPrevious(SPCreateState spCreateNextState)
      : CStateEvtId("state-return-to-previous")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CStateReturnToPrevious, HandlerEvt2), //< the event handler
                    spCreateNextState,                                         //< next state, provided as a constructor parameter
                    2                                                  //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
   }
   
   CStateReturnToPrevious()
      : CStateEvtId("state-return-to-previous")
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CStateReturnToPrevious, HandlerEvt2), //< the event handler
                    NO_STATE_CHANGE,                                   //< the state transition: stay in this state
                    2                                                  //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
   }
   
   ~CStateReturnToPrevious(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
   }
   
public:
   void HandlerEvt2(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine with a current state and
 ** a default state.
 **
 ***************************************************************************************/
int main (void)
{
   //EnableSerialLogDebug();
   LogInfo("[%s][%u] restore-to-previous-state demo in\n", __FUNCTION__, __LINE__);
   
   //create a local scope so that the state machine is
   //destructed when it has handled the event
   try {
      //create the state machine
      LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
      CStateMachine stateMachine(
                                 "state-machine",   //< name used for logging
                                 ToState<CState1>() //< used by the state machine to create the initial state
                                 );

      //handle event
      //(local scoping event data)
      LogInfo("[%s][%u] send first event\n", __FUNCTION__, __LINE__);
      {
         int iEvtData = 5;
         stateMachine.EventHandle(&iEvtData, 1);
      }

      //handle event
      //(local scoping event data)
      LogInfo("[%s][%u] send second event\n", __FUNCTION__, __LINE__);
      {
         int iEvtData = 10;
         stateMachine.EventHandle(&iEvtData, 2);
      }

      //finished sending events
      LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] default-state demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] restore-to-previous-state demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

