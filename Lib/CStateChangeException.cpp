/** @file
 ** @brief The CStateChangeException definition.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "Include/CStateChangeException.h"

namespace ILULibStateMachine {
   /** Constructor.
    **/
   CStateChangeException::CStateChangeException(const std::string& whatArg, SPCreateState spCreateState)
      : std::runtime_error(whatArg)
      , m_spCreateState(spCreateState)
   {
   }

   /** Destructor.
    **
    ** Required because the base class sets 'throw' on its destructor.
    **/
   CStateChangeException::~CStateChangeException() throw()
   {
   }

   /** Get the CCreateState describing the requested state transition by the exception.
    **
    ** @return the SPCreateState describing the requested state transition by the exception.
    **/
   SPCreateState CStateChangeException::GetCreateState() const
   {
      return m_spCreateState;
   }
} // namespace ILULibStateMachine

