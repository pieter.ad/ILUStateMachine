/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CStateChangeException.h"
#include "ToState.h"

#include "CState1.h"
#include "CState2.h"

namespace StateMachineRoot {
   namespace Internal {
      void CState1::HandlerEvt1(const LibEvents::CEventData* const pData)
      {
          ILULibStateMachine::LogInfo("[%s][%u] CState1 glob data [%u] state data [%d]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal, pData->m_iVal);
      }
      
      void CState1::HandlerEvt2(const LibEvents::CEventData* const pData)
      {
          ILULibStateMachine::LogInfo("[%s][%u] CState1 glob data [%u] state data [%d]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal, pData->m_iVal);
      }

      void CState1::HandlerEvt5(const LibEvents::CEventData* const pData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] CState1 glob data [%u] state data [%d] --> [%d]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal, pData->m_iVal, (0 != pData->m_iVal % 2));
         if(0 != pData->m_iVal % 2) {
            return;
         }
         throw std::runtime_error("throw runtime error");
      }

      void CState1::HandlerEvt6(const LibEvents::CEventData* const pData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] CState1 glob data [%u] state data [%d] --> [%d]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal, pData->m_iVal, (0 != pData->m_iVal % 2));
         if(0 != pData->m_iVal % 2) {
            return;
         }
         throw ILULibStateMachine::CStateChangeException("throw CStateChangeException", ILULibStateMachine::ToState<CState2, CData>());
      }

      void CState1::HandlerEvt7(const LibEvents::CEventData* const pData)
      {
         ILULibStateMachine::LogInfo("[%s][%u] CState1 glob data [%u] state data [%d]\n", __FUNCTION__, __LINE__, m_spData->m_uiGlobVal, pData->m_iVal);
      }
   };
};
   
