/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2038 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __DemoNestedStateMachineChild_CState3_H__
#define __DemoNestedStateMachineChild_CState3_H__

#include "CStateEvtId.h"

#include "CEventChildData.h"

#include "CData.h"

namespace DemoNestedStateMachineChild {
   namespace Internal {
      class CState3 : public ILULibStateMachine::CStateEvtId {
         public:
                    CState3(SPData spData);
                    ~CState3(void);

         public:
            void    HandlerEvt3(const DemoNestedStateMachineEvents::CEventChildData* const pEvtData);

         private:
            SPData  m_spData;
      };
   };
};

#endif //__DemoNestedStateMachineChild_CState3_H__

