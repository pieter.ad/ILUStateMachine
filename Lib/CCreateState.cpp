/** @file
 ** @brief The CCreateState definition.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CCreateState.h"
#include "CState.h"

namespace ILULibStateMachine {
   /** Constructor: indicates whether the CreateState function can be called or not
    **/
   CCreateState::CCreateState(const bool bHasToCallCreateStateWithData)
      : m_bHasToCallCreateStateWithData(bHasToCallCreateStateWithData)
   {
   }

   /** Virtual destructor
    **/
   CCreateState::~CCreateState(void)
   {
   }

   /** Check if the virtual CreateState function without parameter in the 
    ** CCreateState class can be called to create the state.
    **/
   bool CCreateState::HasToCallCreateStateWithData(void) const
   {
      return m_bHasToCallCreateStateWithData;
   }
} // namespace ILULibStateMachine

