/** @file
 ** @brief The CState declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CState__H__
#define __ILULibStateMachine_CState__H__

#include <string>

#include "TypedefsEvent.h"

namespace ILULibStateMachine {
   //forward declarations
   //(avoiding recursive includes)
   class CStateMachine;

   /** @brief Base class for all states in a state machine.
    **
    ** Used by the state machine engine to keep track of the current and the default state.
    **
    ** The FCreateState function returns a pointer to a state instance.
    **
    ** SPStateMachineData is NOT a member as this would require casting CStateMachineData to the actual
    ** data class whenever it is used. Thus it is stored directly in the derived state classes instead.
    **/
   class CState {
      friend class CStateMachine; ///< Grant access to EventGetMap and EventTypeGetMap
      
      public:
                                          CState(const char* const szName);
         virtual                          ~CState(void);

      public:
         const std::string&               GetName(void) const;

      protected:
         template <class TEventData> void EventTypeRegisterBase(
            const std::string&                                                        strEventType,
            TYPESEL::function<void(SPEventBase spEventBase, const TEventData* const)> typeHandler,
            SPCreateState                                                             spCreateState   
            );
         template <class TEventData> void EventRegisterBase(
            TYPESEL::function<void(const TEventData* const)> unguaredHandler,
            SPCreateState                                    spCreateState  ,   
            SPEventBase                                      spEventBase    
            );
         template <class TEventData> bool EventRegisterBase(
            TYPESEL::function<bool(const TEventData* const)> guard        ,
            TYPESEL::function<void(const TEventData* const)> handler      ,
            SPCreateState                                    spCreateState,   
            SPEventBase                                      spEventBase    
            );

      private:
         EventMap*                        EventGetMap();
         const EventMap*                  EventGetMap() const;
         EventTypeMap*                    EventTypeGetMap();
         const EventTypeMap*              EventTypeGetMap() const;

      private:
         const std::string                m_strName;        ///< State name, used for logging only.
         EventMap                         m_EventMap;       //< Map of registered event handlers.
         EventTypeMap                     m_EventTypeMap;   //< Map of registered event-type handlers.
   };
}

//include the class template function definitions.
#include "CStateImpl.h"

#endif //__ILULibStateMachine_CState__H__

