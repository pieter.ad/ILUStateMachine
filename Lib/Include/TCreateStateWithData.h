/** @file
 ** @brief The TCreateStateWithData declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_TCreateStateWithData__H__
#define __ILULibStateMachine_TCreateStateWithData__H__

#include "CCreateState.h"

namespace ILULibStateMachine {
   /** Create state class to create a state with a state machine data parameter.
    **/
   template<class TData> class TCreateStateWithData : public CCreateState {
   public:
      /** Constructor setting a state create function for a state with a state machine data parameter.
       **/
      TCreateStateWithData(TYPESEL::function<CState*(TYPESEL::shared_ptr<TData>)> fCreateStateWithData)
         : CCreateState(true)
         , m_fCreateStateWithData(fCreateStateWithData)
      {
      }
 
   public:
      /** Implement the pure CreateState function: throw an exception since creating a state with this class
       ** requires a TData parameter.
       **/
      virtual CState* CreateState(void) const
      {
         throw std::runtime_error("Cannot call 'CreateState(void)' on TCreateStateWithData");
      }
      
      /** Implement CreateState function with a TData parameter: call the provided function in the constructor
       ** to construct the state.
       **/
      virtual CState* CreateState(TYPESEL::shared_ptr<TData> spData) const
      {
         return m_fCreateStateWithData(spData);
      };

   private:
      TYPESEL::function<CState*(TYPESEL::shared_ptr<TData>)> m_fCreateStateWithData; //< The function to be called to create the state.
   };
}

#endif //__ILULibStateMachine_TCreateStateWithData__H__

