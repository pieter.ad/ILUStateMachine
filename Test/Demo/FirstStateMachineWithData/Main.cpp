/** @file
 ** @brief 1-file state machine demo using a state machine data instance
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Track statemachine internals with some ugly global variables.
 ** After all, it is just a test application...
 **
 ***************************************************************************************/
namespace {
   bool g_bSPDataConstructed        = false;
   bool g_bSPDataDestructed         = false;
   bool g_bState1Constructed        = false;
   bool g_bState1Destructed         = false;
   bool g_bState1EventHandlerCalled = false;
   bool g_bState2Constructed        = false;
   bool g_bState2Destructed         = false;
   bool g_bState2EventHandlerCalled = false;
   int  g_iEventHandlerDataValue    = 0;
   int  g_iDataValue                = 0;
   int  g_iDataSetCnt               = 0;
};

/****************************************************************************************
 ** 
 ** State machine data class declaration.
 ** One instance of this class is created and provided to all states in their 
 ** constructors.
 **
 ***************************************************************************************/
class CData {
public:
   CData(void)
      : m_iValue(0)
   {
      LogInfo("[%s][%u] state machine data instance constructed with value [%d]\n", __FUNCTION__, __LINE__, m_iValue);
      g_bSPDataConstructed = true;
   }

   ~CData(void)
   {
      LogInfo("[%s][%u] state machine data instance destructed with value [%d]\n", __FUNCTION__, __LINE__, m_iValue);
      g_bSPDataDestructed = true;
   }
   
public:
   int Get(void) const
   {
      return m_iValue;
   }
   
  void Set(const int iValue)
   {
      ++g_iDataSetCnt;
      m_iValue = iValue;
   }
   
private:
   int m_iValue;
};
typedef TYPESEL::shared_ptr<CData> SPData;

/****************************************************************************************
 ** 
 ** Help check functions.
 **
 ***************************************************************************************/
void CheckStatus(
                 const bool   bSPDataConstructed,
                 const bool   bSPDataDestructed,
                 const bool   bState1Constructed,
                 const bool   bState1Destructed,
                 const bool   bState1EventHandlerCalled,
                 const bool   bState2Constructed,
                 const bool   bState2Destructed,
                 const bool   bState2EventHandlerCalled,
                 const int    iEventHandlerDataValue,
                 const int    iDataValue,
                 const int    iDataSetCnt,
                 const SPData spData,
                 const int    iLine
                 )
{
   //check SPData construction
   if(bSPDataConstructed != g_bSPDataConstructed) {
      stringstream ss;
      ss << "SPData construction at [" << iLine << "] not as expected: value [" << g_bSPDataConstructed << "], expected  [" << bSPDataConstructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check SPData construction
   if(bSPDataDestructed != g_bSPDataDestructed) {
      stringstream ss;
      ss << "SPData desstruction at [" << iLine << "] not as expected: value [" << g_bSPDataDestructed << "], expected  [" << bSPDataDestructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 1 construction
   if(bState1Constructed != g_bState1Constructed) {
      stringstream ss;
      ss << "State 1 construction at [" << iLine << "] not as expected: value [" << g_bState1Constructed << "], expected  [" << bState1Constructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 1 destruction
   if(bState1Destructed != g_bState1Destructed) {
      stringstream ss;
      ss << "State 1 destruction at [" << iLine << "] not as expected: value [" << g_bState1Destructed << "], expected  [" << bState1Destructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check state 1 handler
   if(bState1EventHandlerCalled != g_bState1EventHandlerCalled) {
      stringstream ss;
      ss << "State 1 handler at [" << iLine << "] not called as expected: called [" << g_bState1EventHandlerCalled << "], expected  [" << bState1EventHandlerCalled << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 2 construction
   if(bState2Constructed != g_bState2Constructed) {
      stringstream ss;
      ss << "State 2 construction at [" << iLine << "] not as expected: value [" << g_bState2Constructed << "], expected  [" << bState2Constructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check state 2 destruction
   if(bState2Destructed != g_bState2Destructed) {
      stringstream ss;
      ss << "State 2 destruction at [" << iLine << "] not as expected: value [" << g_bState2Destructed << "], expected  [" << bState2Destructed << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check state 2 handler
   if(bState2EventHandlerCalled != g_bState2EventHandlerCalled) {
      stringstream ss;
      ss << "State 2 handler at [" << iLine << "] not called as expected: called [" << g_bState2EventHandlerCalled << "], expected  [" << bState2EventHandlerCalled << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check event handler data value
   if(iEventHandlerDataValue != g_iEventHandlerDataValue) {
      stringstream ss;
      ss << "Event handler data value mismatch at line [" << iLine << "]: value [" << g_iEventHandlerDataValue << "], expected [" << iEventHandlerDataValue << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }
   
   //check data value
   if((nullptr != spData.get()) && (iDataValue != spData->Get())) {
      stringstream ss;
      ss << "Data value mismatch at line [" << iLine << "]: value [" << spData->Get() << "], expected [" << iDataValue << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //check data set count
   if(iDataSetCnt != g_iDataSetCnt) {
      stringstream ss;
      ss << "Data value mismatch at line [" << iLine << "]: cnt [" << g_iDataSetCnt << "], expected count [" << iDataSetCnt << "] (line [" << __LINE__ << "])";
      throw runtime_error(ss.str());
   }

   //reset
   g_bSPDataConstructed        = false;
   g_bSPDataDestructed         = false;
   g_bState1Constructed        = false;
   g_bState1Destructed         = false;
   g_bState1EventHandlerCalled = false;
   g_bState2Constructed        = false;
   g_bState2Destructed         = false;
   g_bState2EventHandlerCalled = false;
   g_iEventHandlerDataValue    = 0;
   g_iDataValue                = 0;
   g_iDataSetCnt               = 0;
}

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CState2;

/****************************************************************************************
 ** 
 ** First state
 **
 ** This state has 1 event handler. The event ID is an integer (value 1). It calls the
 ** registered handler and then switches to the second state.
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1(SPData spData)
      : CStateEvtId("state-1")
      , m_spData(spData)
   {
      //register event handlers
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1), //< the event handler
                    ToState<CState2, CData>(),          //< the state transition: switch to state-2 when the handler returns
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created (current data: [%d])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->Get());
      spData->Set(1);
      g_bState1Constructed = true;
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed (current data: [%d])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->Get());
      g_bState1Destructed = true;
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      m_spData->Set(*pEvtData);
      g_bState1EventHandlerCalled = true;
      g_iEventHandlerDataValue    = *pEvtData;
   }

private:
   SPData m_spData;
};

/****************************************************************************************
 ** 
 ** Second state.
 **
 ** This state has 1 event handler. The event ID is an integer (value 1). It calls the
 ** registered handler and then remains in this state.
 **
 ***************************************************************************************/
/** Class definition of state-2
 **/
class CState2 : public CStateEvtId {
public:
   CState2(SPData spData)
      : CStateEvtId("state-2")
      , m_spData(spData)
   {
      //register handlers
      EventRegister(
                    HANDLER(int, CState2, HandlerEvt1), //< the event handler
                    NO_STATE_CHANGE,                    //< the state transition: stay in this state
                    1                                   //< the event ID (type integer)
                    );
      LogInfo("[%s][%u] [%s] created (current data: [%d])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->Get());
      spData->Set(2);
      g_bState2Constructed = true;
   }
   
   ~CState2(void)
   {
      LogInfo("[%s][%u] [%s] destructed (current data: [%d])\n", __FUNCTION__, __LINE__, GetName().c_str(), m_spData->Get());
      g_bState2Destructed = true;
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      m_spData->Set(*pEvtData);
      g_bState2EventHandlerCalled = true;
      g_iEventHandlerDataValue    = *pEvtData;
   }
   
private:
   SPData m_spData;
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine and
 ** sends 1 event to it.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] first-statemachine-with-data demo in\n", __FUNCTION__, __LINE__);
   
   try {
      //create a local scope so that the state machine is
      //destructed when it has handled the event
      {
         //check
         CheckStatus(false, false, false, false, false, false, false, false, 0, 0, 0, SPData(), __LINE__);

         //create state machine data so it can be accessed in the tests
         SPData spData(new CData());
         
         //check
         CheckStatus(true,  false, false, false, false, false, false, false, 0, 0, 0, spData, __LINE__);
         
         //create the state machine
         LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
         TStateMachineWithData<CData> stateMachine(
                                                   "state-machine",          //< name used for logging
                                                   spData,                   //< state machine takes ownership of the data instance and will destruct it
                                                   ToState<CState1, CData>() //< used by the state machine to create the initial state
                                                   );
         
         //check
         CheckStatus(false, false, true,  false, false, false, false, false, 0, 1, 1, spData, __LINE__);
         
         //inject unhandled event ID in state 1
         LogInfo("[%s][%u] send unhandled event\n", __FUNCTION__, __LINE__);
         {
            const int iEvtData = 1942;
            stateMachine.EventHandle(&iEvtData, 98);
         }
         
         //check
         CheckStatus(false, false, false, false, false, false, false, false, 0, 1, 0, spData, __LINE__);
         
         //handle first event
         const int iEvtData1 = 1971;
         LogInfo("[%s][%u] send first event with data [%d]\n", __FUNCTION__, __LINE__, iEvtData1);
         {
            stateMachine.EventHandle(&iEvtData1, 1);
         }
         
         //check
         CheckStatus(false, false, false, true,  true,  true,  false, false, iEvtData1, 2, 2, spData, __LINE__);
         
         //inject unhandled event ID in state 2
         LogInfo("[%s][%u] send unhandled event\n", __FUNCTION__, __LINE__);
         {
            const int iEvtData = 1941;
            stateMachine.EventHandle(&iEvtData, 99);
         }
         
         //check
         CheckStatus(false, false, false, false, false, false, false, false, 0, 2, 0, spData, __LINE__);
         
         //handle second event
         const int iEvtData2 = 2018;
         LogInfo("[%s][%u] send second event\n", __FUNCTION__, __LINE__);
         {
            stateMachine.EventHandle(&iEvtData2, 1);
         }
         
         //check
         CheckStatus(false, false, false, false, false, false, false, true,  iEvtData2, iEvtData2, 1, spData, __LINE__);
         
         //finished sending events
         LogInfo("[%s][%u] sending events done\n", __FUNCTION__, __LINE__);
      }//state machine goes out-of-scope here

      //check
      CheckStatus(false, true,  false, false, false, false, true,  false, 0, 0, 0, SPData(), __LINE__);
      
      //all tests OK
      LogInfo("[%s][%u] all tests OK :-)\n", __FUNCTION__, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] first-statemachine-with-data demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   //finish
   LogInfo("[%s][%u] first-statemachine-with-data demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

