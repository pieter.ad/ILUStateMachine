/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>

//include the statemachine library
#include "StateMachine.h"

#include "CEventChildData.h"
#include "CEventRootData.h"
#include "EEventChild.h"
#include "EEventRoot.h"

#include "StateMachineRoot.h"

using namespace std;
using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineEvents;
using namespace DemoNestedStateMachineRoot;

/****************************************************************************************
 ** 
 ** This is the main function of the nested-statemachine demo.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] nested-statemachine demo in\n", __FUNCTION__, __LINE__);
   
   try {
      //create a local scope so that the state machine is
      //destructed when it has handled the event
      {
         //check
         RootCheckStatus(false, false, false, false, false, false, false, false, false, __LINE__);
         ChildCheckStatus(0);
         
         //create the state machine
         LogInfo("[%s][%u] create root state machine\n", __FUNCTION__, __LINE__);
         SPStateMachine spStateMachine = CreateStateMachine();
         
         //check
         RootCheckStatus(true,  false, false, false, false, false, false, false, false, __LINE__);
         ChildCheckStatus(0);
         
         //send child-state-machine event
         //this is unhandled in root state 1
         {
            const EEventChild     eventId  (EEventChild1);
            const CEventChildData eventData("child event 1");
            LogInfo("[%s][%u] send child-state-machine event ID [%u] with data [%s]\n", __FUNCTION__, __LINE__, eventId, eventData.Get().c_str());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, false, false, false, false, false, false, false, false, __LINE__);
         ChildCheckStatus(0);
         
         //send root-state-machine event
         //this will result in a root-state-machine state change from state 1 to state 2
         {
            const EEventRoot     eventId  (EEventRoot1);
            const CEventRootData eventData(1);
            LogInfo("[%s][%u] send root-state-machine event ID [%u] with data [%d]\n", __FUNCTION__, __LINE__, eventId, eventData.Get());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, true,  true,  true,  false, false, false, false, false, __LINE__);
         ChildCheckStatus(1, true,  false, false, false, false, false, false, false, false, __LINE__);
         
         //send child-state-machine event
         //this will be forwarded by the root-state-machine in state 2 to the child-state-machine
         {
            const EEventChild     eventId  (EEventChild1);
            const CEventChildData eventData("child event 1");
            LogInfo("[%s][%u] send child-state-machine event ID [%u] with data [%s]\n", __FUNCTION__, __LINE__, eventId, eventData.Get().c_str());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, false, false, false, false, true,  false, false, false, __LINE__);
         ChildCheckStatus(1, false, true,  true,  true, false, false, false, false, false, __LINE__);
         
         //send child-state-machine event
         //this will be forwarded by the root-state-machine in state 2 to the child-state-machine
         {
            const EEventChild     eventId  (EEventChild2);
            const CEventChildData eventData("child event 2");
            LogInfo("[%s][%u] send child-state-machine event ID [%u] with data [%s]\n", __FUNCTION__, __LINE__, eventId, eventData.Get().c_str());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, false, false, false, false, true,  false, false, false, __LINE__);
         ChildCheckStatus(1, false, false, false, false, true,  true,  true,  false, false, __LINE__);
         
         //send child-state-machine event
         //this will be forwarded by the root-state-machine in state 2 to the child-state-machine
         //this will also finish the child-state-machine, which will trigger a state transition to state 3 in the root-state-machine
         {
            const EEventChild     eventId  (EEventChild3);
            const CEventChildData eventData("child event 3");
            LogInfo("[%s][%u] send child-state-machine event ID [%u] with data [%s]\n", __FUNCTION__, __LINE__, eventId, eventData.Get().c_str());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, false, false, false, true,  true,  true,  false, false, __LINE__);
         ChildCheckStatus(0);
         
         //send root-state-machine event
         //the root-state-machine will handle it but will stay in the same state
         {
            const EEventRoot     eventId  (EEventRoot3);
            const CEventRootData eventData(3);
            LogInfo("[%s][%u] send root-state-machine event ID [%u] with data [%d]\n", __FUNCTION__, __LINE__, eventId, eventData.Get());
            spStateMachine->EventHandle(&eventData, eventId);
         }
         
         //check
         RootCheckStatus(false, false, false, false, false, false, false, false, true,  __LINE__);
         ChildCheckStatus(0);
         
         //finished sending events
         LogInfo("[%s][%u] sending events done, root state machine %s finished\n", __FUNCTION__, __LINE__, spStateMachine->HasFinished() ? "has" : "not");
      }

      //check
      RootCheckStatus(false, false, false, false, false, false, false, true, false,  __LINE__);
      ChildCheckStatus(0);         
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] nested-statemachine demo error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] nested-statemachine demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

