/** @file
 ** @brief The CCreateState declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CCreateState__H__
#define __ILULibStateMachine_CCreateState__H__

#include "Types.h"

namespace ILULibStateMachine {
   //forward declarations
   //(avoiding recursive includes)
   class CState;

   /** @brief Base class for all state-creation classes.
    **
    ** It has 2 major flavours:
    ** - The state can be created by calling the CreateState function
    **   without data parameter on the CCreate instance;
    ** - The state can be created by calling a CreateState function in
    **   a derived class that has a parameter of which the type is unknown
    **   in the base class.
    ** This difference is managed by the m_bHasToCallCreateStateWithData
    ** member.
    **/
   class CCreateState {
      public:
         virtual             ~CCreateState(void);
 
      public:
         virtual CState*     CreateState(void) const = 0; ///< Pure virtual function to actually create the state
         bool                HasToCallCreateStateWithData(void) const;

      protected:
                             CCreateState(const bool bHasToCallCreateStateWithData = false);
      
      private:
         bool                m_bHasToCallCreateStateWithData; ///< Can CreateState without parameters be called on the CCreateClass or not.   
   };

   /** Define a shared pointer to CCreateState.
    **/
   typedef TYPESEL::shared_ptr<CCreateState> SPCreateState;
}

/** Use this macro in the event registration calls to indicate that the event does not result in a state change.
 **/
#define NO_STATE_CHANGE SPCreateState()

#endif //__ILULibStateMachine_CCreateState__H__

