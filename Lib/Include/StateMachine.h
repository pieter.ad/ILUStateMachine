/** @file
 ** @brief StateMachine all-include file.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_StateMachine__H__
#define __ILULibStateMachine_StateMachine__H__

#include "CCreateState.h"
#include "CCreateStateFinished.h"
#include "CEventBase.h"
#include "CHandleEventInfoBase.h"
#include "CLogIndent.h"
#include "CSPEventBaseSort.h"
#include "CState.h"
#include "CStateChangeException.h"
#include "CStateEvtId.h"
#include "CStateEvtIdImpl.h"
#include "CStateMachine.h"
#include "EEvtSubNotSet.h"
#include "Logging.h"
#include "TCreateStateWithData.h"
#include "TEventEvtId.h"
#include "TEventEvtIdImpl.h"
#include "THandleEventInfo.h"
#include "THandleEventTypeInfo.h"
#include "ToState.h"
#include "TStateMachineWithData.h"
#include "TypedefsEvent.h"
#include "Types.h"

#endif //__ILULibStateMachine_StateMachine__H__

