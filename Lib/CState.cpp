/** @file
 ** @brief The CState definition. 
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "CState.h"

namespace ILULibStateMachine {
   /** Constructor.
    **/
   CState::CState(
      const char* const szName  ///< State name, used for logging only.
      )
      : m_strName(szName)
      , m_EventMap()
      , m_EventTypeMap()
   {
   }

   /** Destructor to make this a virtual class.
    **/
   CState::~CState()
   {
   };

   /** Get the state name.
    **
    ** @return the state name.
    **/
   const std::string& CState::GetName() const
   {
      return m_strName;
   }

   /** Get a reference to the event map.
    **/
   EventMap* CState::EventGetMap()
   {
      return &m_EventMap;
   }

   /** Get a const reference to the event map.
    **/
   const EventMap* CState::EventGetMap() const
   {
      return &m_EventMap;
   }

   /** Get a reference to the event-type map.
    **/
   EventTypeMap* CState::EventTypeGetMap()
   {
      return &m_EventTypeMap;
   }

   /** Get a const reference to the event-type map.
    **/
   const EventTypeMap* CState::EventTypeGetMap() const
   {
      return &m_EventTypeMap;
   }
} // namespace ILULibStateMachine

