/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Track statemachine internals with some ugly global variables.
 ** After all, it is just a test application...
 **
 ***************************************************************************************/
namespace {
   bool g_bState1Constructed                      = false;
   bool g_bState1Destructed                       = false;
   bool g_bState1EventHandlerIntCalled            = false;
   bool g_bState1EventHandlerSEventDataCalled     = false;
   bool g_bState1TypeEventHandlerIntCalled        = false;
   bool g_bState1TypeEventHandlerSEventDataCalled = false;

   void TrackingClear(void)
   {
      g_bState1EventHandlerIntCalled            = false;
      g_bState1EventHandlerSEventDataCalled     = false;
      g_bState1TypeEventHandlerIntCalled        = false;
      g_bState1TypeEventHandlerSEventDataCalled = false;
   }

   void TrackingCheck(
                      const int iLine,
                      const bool bState1EventHandlerIntExpected,
                      const bool bState1EventHandlerSEventDataExpected,
                      const bool bState1TypeEventHandlerIntExpected,
                      const bool bState1TypeEventHandlerSEventDataExpected
                      )
   {
      if(bState1EventHandlerIntExpected != g_bState1EventHandlerIntCalled) {
         stringstream ss;
         ss << "Mismatch for 'State1EventHandlerInt' at [" << iLine << "] not called as expected: called [" << g_bState1EventHandlerIntCalled << "], expected  [" << bState1EventHandlerIntExpected << "]";
         throw runtime_error(ss.str());
      }
      if(bState1EventHandlerSEventDataExpected != g_bState1EventHandlerSEventDataCalled) {
         stringstream ss;
         ss << "Mismatch for 'State1EventHandlerSEventData' at [" << iLine << "] not called as expected: called [" << g_bState1EventHandlerSEventDataCalled << "], expected  [" << bState1EventHandlerSEventDataExpected << "]";
         throw runtime_error(ss.str());
      }
      if(bState1TypeEventHandlerIntExpected != g_bState1TypeEventHandlerIntCalled) {
         stringstream ss;
         ss << "Mismatch for 'State1TypeEventHandlerInt' at [" << iLine << "] not called as expected: called [" << g_bState1TypeEventHandlerIntCalled << "], expected  [" << bState1TypeEventHandlerIntExpected << "]";
         throw runtime_error(ss.str());
      }
      if(bState1TypeEventHandlerSEventDataExpected != g_bState1TypeEventHandlerSEventDataCalled) {
         stringstream ss;
         ss << "Mismatch for 'State1TypeEventHandlerSEventData' at [" << iLine << "] not called as expected: called [" << g_bState1TypeEventHandlerSEventDataCalled << "], expected  [" << bState1TypeEventHandlerSEventDataExpected << "]";
         throw runtime_error(ss.str());
      }

      TrackingClear();
   }
};

/****************************************************************************************
 ** 
 ** Define an enum with event ID's
 **
 ***************************************************************************************/
enum EEventIds{
   EEventIds1,
   EEventIds2,
   EEventIds3,
};

/****************************************************************************************
 ** 
 ** Define an event data struct
 **
 ***************************************************************************************/
struct SEventData {
   std::string m_strInfo;
   int         m_iInfo;

   SEventData(const char* const szInfo, const int iInfo)
      : m_strInfo(szInfo)
      , m_iInfo(iInfo)
   {
   }
};

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;

/****************************************************************************************
 ** 
 ** First state
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      //handler for event type EEventIds with event data type 'int'
      EventRegister(
                    HANDLER(int, CState1, HandlerEvt1),        //< the event handler
                    NO_STATE_CHANGE,                           //< the state transition: stay in this state
                    EEventIds1                                 //< the event ID (type integer)
                    );
      //handler for event type EEventIds with event data type 'SEventData'
      EventRegister(
                    HANDLER(SEventData, CState1, HandlerEvt2), //< the event handler
                    NO_STATE_CHANGE,                           //< the state transition: stay in this state
                    EEventIds2                                 //< the event ID (type integer)
                    );
      //type handler for event type EEventIds with event data type 'int'
      EventTypeRegister(
                        HANDLER_TYPE(EEventIds, int, CState1, HandlerEvtDataTypeInt),
                        NO_STATE_CHANGE
                        );
      //type handler for event type EEventIds with event data type 'SEventData'
      EventTypeRegister(
                        HANDLER_TYPE(EEventIds, SEventData, CState1, HandlerEvtDataTypeSEventData),
                        NO_STATE_CHANGE
                        );

      LogInfo("[%s][%u] [%s] created\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Constructed = true;
   }
   
   ~CState1(void)
   {
      LogInfo("[%s][%u] [%s] destructed\n", __FUNCTION__, __LINE__, GetName().c_str());
      g_bState1Destructed = true;
   }
   
public:
   void HandlerEvt1(const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData);
      g_bState1EventHandlerIntCalled            = true;
   }

   void HandlerEvt2(const SEventData* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%s][%d]\n", __FUNCTION__, __LINE__, GetName().c_str(), pEvtData->m_strInfo.c_str(), pEvtData->m_iInfo);
      g_bState1EventHandlerSEventDataCalled     = true;
   }

   void HandlerEvtDataTypeInt(SPEventBase spEventBase, const int* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] type-handle event with data [%d] (type ID [%s:%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), *pEvtData, spEventBase->GetIdType().c_str(), spEventBase->GetDataType().c_str());
      g_bState1TypeEventHandlerIntCalled        = true;
   }

   void HandlerEvtDataTypeSEventData(SPEventBase spEventBase, const SEventData* const pEvtData)
   {
      LogInfo("[%s][%u] [%s] handle event with data [%s][%d] (type ID [%s:%s])\n", __FUNCTION__, __LINE__, GetName().c_str(), pEvtData->m_strInfo.c_str(), pEvtData->m_iInfo, spEventBase->GetIdType().c_str(), spEventBase->GetDataType().c_str());
      g_bState1TypeEventHandlerSEventDataCalled = true;
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine and
 ** sends 1 event to it.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] handle-same-event-type-different-event-date demo in\n", __FUNCTION__, __LINE__);
   
   //create a local scope so that the state machine is
   //destructed when it has handled the event
   try {
      {
         //create the state machine
         LogInfo("[%s][%u] create state machine\n", __FUNCTION__, __LINE__);
         CStateMachine stateMachine(
                                    "state-machine",   //< name used for logging
                                    ToState<CState1>() //< used by the state machine to create the initial state
                                    );

         //handle event
         {
            const int iEvtData = 5;
            LogInfo("[%s][%u] send first event with data [%d]\n", __FUNCTION__, __LINE__, iEvtData);
            stateMachine.EventHandle(&iEvtData, EEventIds1);
         }
         TrackingCheck(__LINE__, true, false, false, false);

         //handle event
         {
            const SEventData evtData("evt data info", 11);
            LogInfo("[%s][%u] send event with data [%s][%d]\n", __FUNCTION__, __LINE__, evtData.m_strInfo.c_str(), evtData.m_iInfo);
            stateMachine.EventHandle(&evtData, EEventIds2);
         }
         TrackingCheck(__LINE__, false, true, false, false);

         //type-handle event
         {
            const int iEvtData = 15;
            LogInfo("[%s][%u] send with data [%d]\n", __FUNCTION__, __LINE__, iEvtData);
            stateMachine.EventHandle(&iEvtData, EEventIds3);
         }
         TrackingCheck(__LINE__, false, false, true, false);

         //type-handled event
         {
            const SEventData evtData("evt data info", 21);
            LogInfo("[%s][%u] send event with data [%s][%d]\n", __FUNCTION__, __LINE__, evtData.m_strInfo.c_str(), evtData.m_iInfo);
            stateMachine.EventHandle(&evtData, EEventIds3);
         }
         TrackingCheck(__LINE__, false, false, false, true);

         //unhandled event
         {
            const std::string evtData("evt data string");
            LogInfo("[%s][%u] send event with data [%s]\n", __FUNCTION__, __LINE__, evtData.c_str());
            stateMachine.EventHandle(&evtData, EEventIds3);
         }
         TrackingCheck(__LINE__, false, false, false, false);
      }
      
      //check that state 1 was constructed
      if(!g_bState1Constructed) {
         throw runtime_error("State1 not constructed");
      }
      
      //check that state 1 was destructed
      if(!g_bState1Destructed) {
         throw runtime_error("State1 not destructed");
      }
      
      //all tests OK
      LogInfo("[%s][%u] all tests OK :-)\n", __FUNCTION__, __LINE__);
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] handle-same-event-type-different-event-date demo error :-( : %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] handle-same-event-type-different-event-date demo out\n", __FUNCTION__, __LINE__);
   return 0;
}

