/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "ToState.h"
#include "TStateMachineWithData.h"

#include "CData.h"
#include "CState1.h"
#include "CStateDefault.h"
#include "StateMachineRoot.h"

//StateMachineRoot common implementation
namespace StateMachineRoot {
   ILULibStateMachine::SPStateMachine CreateStateMachine(const unsigned int uiValue)
   {
      //create & return state machine
      return ILULibStateMachine::SPStateMachine(
         new ILULibStateMachine::TStateMachineWithData<Internal::CData>(
            "state-machine-root",                                                    //< name used for logging
            TYPESEL::shared_ptr<Internal::CData>(new Internal::CData(uiValue)),      //< instantiate state machine data                            
            ILULibStateMachine::ToState<Internal::CState1,       Internal::CData>(), //< used to create the initial state
            ILULibStateMachine::ToState<Internal::CStateDefault, Internal::CData>()  //< used to create the default state
         )
      );
   }
};

