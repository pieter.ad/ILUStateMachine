/** @file
 ** @brief The CState template function defintions.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CStateImpl__H__
#define __ILULibStateMachine_CStateImpl__H__

#include "stdexcept"

#include "Logging.h"
#include "THandleEventInfo.h"
#include "THandleEventTypeInfo.h"

namespace ILULibStateMachine {
   /** Register an event-type handler.
    **/
   template <class TEventData> 
   void CState::EventTypeRegisterBase(
      const std::string&                                                        strEventType, //< String representation of the event type.
      TYPESEL::function<void(SPEventBase spEventBase, const TEventData* const)> typeHandler,  //< The handler to be registered.
      SPCreateState                                                             spCreateState //< The state transition accompanying this event-type.
      )
   {
      try {
         EventTypeMap* const pMap = EventTypeGetMap();
         const EventTypeMapIt it = pMap->find(strEventType);
         if(pMap->end() == it) {
            LogDebug("Register type event handler for [%s] in [%s]\n",
                     strEventType.c_str(),
                     GetName().c_str()
                     );
            pMap->insert(EventTypePair(strEventType, SPHandleEventInfoBase(new THandleEventTypeInfo<TEventData>(typeHandler, spCreateState))));
         } else {
            //event already in the map
            LogErr("Register type event handler for [%s] in [%s] failed: already registered\n",
                   strEventType.c_str(),
                   GetName().c_str()
                   );
         }
      } catch(std::exception& ex) {
         LogErr("Event type handler registration failed for [%s]: %s\n",
                strEventType.c_str(),
                ex.what()
                );
      } catch(...) {
         LogErr("Event type handler registration failed for [%s]: %s\n",
                strEventType.c_str(),
                "unknown"
                );
      }
   }

   /** Register an unguarded event handler.
    **/
   template <class TEventData> 
   void CState::EventRegisterBase(
      TYPESEL::function<void(const TEventData* const)> unguardedHandler, //< The handler to be registered.
      SPCreateState                                    spCreateState,    //< The state transition accompanying this event-type.
      SPEventBase                                      spEventBase       //< The complete event identification that triggers this handler.
      )
   {
      try {
         EventMap* const pMap = EventGetMap();
         const EventMapIt it = pMap->find(spEventBase);
         if(pMap->end() == it) {
            LogDebug("Register unguarded event handler for [%s] in [%s]\n",
                     spEventBase->GetId().c_str(),
                     GetName().c_str()
                     );
            pMap->insert(EventPair(spEventBase, SPHandleEventInfoBase(new THandleEventInfo<TEventData>(unguardedHandler, spCreateState))));
         } else {
            //event already in the map
            //--> set the default handler
            //    (will throw when the default handler has already been set)
            LogDebug("Set unguarded event handler for [%s] in [%s]\n",
                     spEventBase->GetId().c_str(),
                     GetName().c_str()
                     );
            THandleEventInfo<TEventData>* pHandleEventInfo = dynamic_cast<THandleEventInfo<TEventData>*>(it->second.get());
            if(NULL == pHandleEventInfo) {
               //serious error in the implementation: mismatch in registration
               throw std::runtime_error("IMPLEMENTATION ERROR: registration mismatch found in unguarded event handler");
            }
            pHandleEventInfo->SetUnguardedHandler(unguardedHandler, spCreateState);
         }
      } catch(std::exception& ex) {
         LogErr("Event default handler registration failed for [%s]: %s\n",
                spEventBase->GetId().c_str(),
                ex.what()
                );
      } catch(...) {
         LogErr("Event default handler registration failed for [%s]: %s\n",
                spEventBase->GetId().c_str(),
                "unknown"
                );
      }
   }

   /** Register a guarded event handler.
    **/
   template <class TEventData> 
   bool CState::EventRegisterBase(
      TYPESEL::function<bool(const TEventData* const)> guard,         //< The guard called before the handler. When the guard returns true, the handler is called; when the guard returns false the handler is not called.
      TYPESEL::function<void(const TEventData* const)> handler,       //< The handler to be registered.
      SPCreateState                                    spCreateState, //< The state transition accompanying this event-type.
      SPEventBase                                      spEventBase    //< The complete event identification that triggers this handler.
      )
   {
      try {
         EventMap* const pMap = EventGetMap();
         const EventMapIt it = pMap->find(spEventBase);
         if(pMap->end() == it) {
            //event with the specified ID not yet in the map
            //--> add it with a guarded handler
            LogDebug("Register event guard/handler combo for [%s] in [%s]\n",
                     spEventBase->GetId().c_str(),
                     GetName().c_str()
                     );
            pMap->insert(EventPair(spEventBase, SPHandleEventInfoBase(new THandleEventInfo<TEventData>(guard, handler, spCreateState))));
         } else {
            //event already in the map
            //--> add a guarded handler
            LogDebug("Add event guard/handler combo for [%s] in [%s]\n",
                     spEventBase->GetId().c_str(),
                     GetName().c_str()
                     );
            THandleEventInfo<TEventData>* pHandleEventInfo = dynamic_cast<THandleEventInfo<TEventData>*>(it->second.get());
            if(NULL == pHandleEventInfo) {
               //serious error in the implementation: mismatch in registration
               throw std::runtime_error("IMPLEMENTATION ERROR: registration mismatch found in guarded event handler");
            }
            pHandleEventInfo->AddGuardedHandler(guard, handler, spCreateState);
         }
         return true;
      } catch(std::exception& ex) {
         LogErr("Event guard/handler combo registration failed for [%s]: %s\n",
                spEventBase->GetId().c_str(),
                ex.what()
                );
         return false;
      } catch(...) {
         LogErr("Event guard/handler combo registration failed for [%s]: %s\n",
                spEventBase->GetId().c_str(),
                "unknown"
                );
         return false;
      }
   }
};
   
#endif //__ILULibStateMachine_CStateImpl__H__

