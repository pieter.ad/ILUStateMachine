/** @file
 ** @brief The EEvtSubNotSet template function definition.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_EEvtSubNotSet_H__
#define __ILULibStateMachine_EEvtSubNotSet_H__

namespace ILULibStateMachine {
   /** @brief Definition of the EEvtSubNotSet enum used by TEventEvtId
    ** as default template parameters for the sub-event-ID's.
    **/
   enum EEvtSubNotSet {
   };
};

#endif //__ILULibStateMachine_EEvtSubNotSet_H__

