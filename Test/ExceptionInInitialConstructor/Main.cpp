/** @file
 ** @brief 1-file state machine demo
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>
using namespace std;

//include the statemachine library and make using it easy
#include "StateMachine.h"
using namespace ILULibStateMachine;

/****************************************************************************************
 ** 
 ** Forward declaration of all state classes, 
 ** so they can be used when regestering state transitions.
 **
 ***************************************************************************************/
class CState1;
class CState2;

/****************************************************************************************
 ** 
 ** First state: throws an excpetion in its constructor.
 **
 ** This state has no event handlers.
 **
 ***************************************************************************************/
/** Class definition of state-1
 **/
class CState1 : public CStateEvtId {
public:
   CState1()
      : CStateEvtId("state-1")
   {
      throw runtime_error("CState1 constructor exception");
   }
};

/****************************************************************************************
 ** 
 ** Second state: throws no excpetion in its constructor.
 **
 ** This state has no event handlers.
 **
 ***************************************************************************************/
/** Class definition of state-2
 **/
class CState2 : public CStateEvtId {
public:
   CState2()
      : CStateEvtId("state-2")
   {
      LogInfo("[%s][%u] CState2 constructed\n", __FUNCTION__, __LINE__);
   }
};

/****************************************************************************************
 ** 
 ** This is the main function.
 ** It instantiates the state machine with a current state and
 ** a default state.
 **
 ***************************************************************************************/
int main (void)
{
   EnableSerialLogDebug();
   LogInfo("[%s][%u] exception-in-constructor in\n", __FUNCTION__, __LINE__);

   try {
      //this test should result in an exception
      {
         bool bExCaught = false;
         try {
            //create a state machine without an intial state and without a default state
            //--> the state machine has no state at all and will throw an exception
            //    itself.
            LogInfo("[%s][%u] create state machine without an intial state and without a default state\n", __FUNCTION__, __LINE__);
            CStateMachine stateMachine(
                                       "state-machine", //< name used for logging
                                       SPCreateState()  //< used by the state machine to create the initial state
                                       );
         } catch(std::exception& ex) {
            LogInfo("[%s][%u] expected exception caught: %s\n", __FUNCTION__, __LINE__, ex.what());
            bExCaught = true;
         }
         if(!bExCaught) {
            std::stringstream ss;
            ss << "Expected exception not caught (line: [" << __LINE__ << "]";
            throw runtime_error(ss.str());
         }
      }

      //this test should result in an exception
      {
         bool bExCaught = false;
         try {
            //create a state machine with an intial state and no default state
            //the initial state throws an exception in its constructor
            //--> the state machine has no state at all and will throw an exception
            //    itself.
            LogInfo("[%s][%u] create state machine with an intial state  (throws) and no default state\n", __FUNCTION__, __LINE__);
            CStateMachine stateMachine(
                                       "state-machine",   //< name used for logging
                                       ToState<CState1>() //< used by the state machine to create the initial state
                                       );
         } catch(std::exception& ex) {
            LogInfo("[%s][%u] expected exception caught: %s\n", __FUNCTION__, __LINE__, ex.what());
            bExCaught = true;
         }
         if(!bExCaught) {
            std::stringstream ss;
            ss << "Expected exception not caught (line: [" << __LINE__ << "]";
            throw runtime_error(ss.str());
         }
      }
      
      //this test should result in an exception
      {
         bool bExCaught = false;
         try {
            //create a state machine without an intial state and with default state
            //the default state throws an exception in its constructor
            //--> the state machine has no state at all and will throw an exception
            //    itself.
            LogInfo("[%s][%u] create state machine without an intial state and with a default state (throws)\n", __FUNCTION__, __LINE__);
            CStateMachine stateMachine(
                                       "state-machine",   //< name used for logging
                                       SPCreateState(),   //< used by the state machine to create the initial state
                                       ToState<CState1>() //< used by the state machine to create the default state
                                       );
         } catch(std::exception& ex) {
            LogInfo("[%s][%u] expected exception caught: %s\n", __FUNCTION__, __LINE__, ex.what());
            bExCaught = true;
         }
         if(!bExCaught) {
            std::stringstream ss;
            ss << "Expected exception not caught (line: [" << __LINE__ << "]";
            throw runtime_error(ss.str());
         }
      }

      //this test should result in an exception
      {
         bool bExCaught = false;
         try {
            //create a state machine with an intial state and with default state
            //both the initial state and the default state throws an exception in
            //their constructor
            //--> the state machine has no state at all and will throw an exception
            //    itself.
            LogInfo("[%s][%u] create state machine without an intial state and with a default state (throws)\n", __FUNCTION__, __LINE__);
            CStateMachine stateMachine(
                                       "state-machine",    //< name used for logging
                                       ToState<CState1>(), //< used by the state machine to create the initial state
                                       ToState<CState1>()  //< used by the state machine to create the default state
                                       );
         } catch(std::exception& ex) {
            LogInfo("[%s][%u] expected exception caught: %s\n", __FUNCTION__, __LINE__, ex.what());
            bExCaught = true;
         }
         if(!bExCaught) {
            std::stringstream ss;
            ss << "Expected exception not caught (line: [" << __LINE__ << "]";
            throw runtime_error(ss.str());
         }
      }

      //this test should not result in an exception
      {
         //create a state machine with an intial state and a default state
         //the initial state throws an exception in its constructor
         //since the state machine still has the default state, it will not
         //throw  an exception itself.
         LogInfo("[%s][%u] create state machine with an intial state (throws) and with default state\n", __FUNCTION__, __LINE__);
         CStateMachine stateMachine(
                                    "state-machine",    //< name used for logging
                                    ToState<CState1>(), //< used by the state machine to create the initial state
                                    ToState<CState2>()  //< used by the state machine to create the default state
                                    );
      }
      
      //this test should not result in an exception
      {
         //create a state machine with an intial state and a default state
         //the defaut state throws an exception in its constructor
         //since the state machine still has the intial state, it will not
         //throw  an exception itself.
         LogInfo("[%s][%u] create state machine with an intial state and with default state (throws)\n", __FUNCTION__, __LINE__);
         CStateMachine stateMachine(
                                    "state-machine",    //< name used for logging
                                    ToState<CState2>(), //< used by the state machine to create the default state
                                    ToState<CState1>()  //< used by the state machine to create the initial state
                                    );
      }
   } catch(std::exception& ex) {
      LogInfo("[%s][%u] exception-in-constructor error: %s\n", __FUNCTION__, __LINE__, ex.what());
      return -1;
   }
   
   LogInfo("[%s][%u] exception-in-constructor out\n", __FUNCTION__, __LINE__);
   return 0;
}

