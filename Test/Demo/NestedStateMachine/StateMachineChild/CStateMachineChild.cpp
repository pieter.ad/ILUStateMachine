/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include <stdexcept>

#include "ToState.h"

#include "CState1.h"
#include "CStateMachineChild.h"
#include "StateConstrDestr.h"

using namespace std;
using namespace ILULibStateMachine;
using namespace DemoNestedStateMachineChild::Internal;

namespace DemoNestedStateMachineChild {
   int CStateMachineChild::m_iNbrInstances = 0; 

   CStateMachineChild::CStateMachineChild(void)
      : TStateMachineWithData<CData>("state-machine-child", SPData(new CData("init")), ToState<CState1, CData>())
   {
      ++m_iNbrInstances;
   }
   
   CStateMachineChild::~CStateMachineChild(void)
   {
      --m_iNbrInstances;
   }

   std::string CStateMachineChild::InfoGet(void) const
   {
      return m_spData->InfoGet();
   }

   void CStateMachineChild::CheckStatus(
                                        const int  iNbrInstances,
                                        const bool bState1Constructed,
                                        const bool bState1Destructed,
                                        const bool bState1EventHandlerCalled,
                                        const bool bState2Constructed,
                                        const bool bState2Destructed,
                                        const bool bState2EventHandlerCalled,
                                        const bool bState3Constructed,
                                        const bool bState3Destructed,
                                        const bool bState3EventHandlerCalled,
                                        const int  iLine
                                        )
   {
      //check the number of instances
      if(iNbrInstances != m_iNbrInstances) {
         stringstream ss;
         ss << "Child statemachine instances [" << iLine << "] not as expected: value [" << m_iNbrInstances << "], expected  [" << iNbrInstances << "] (line [" << __LINE__ << "])";
         throw runtime_error(ss.str());
      }

      //if there is not exactly 1 instance, don't check in detail
      if(1 != m_iNbrInstances) {
         return;
      }

      //call internal check function
      return Internal::CheckStatus(
                                   bState1Constructed,
                                   bState1Destructed,
                                   bState1EventHandlerCalled,
                                   bState2Constructed,
                                   bState2Destructed,
                                   bState2EventHandlerCalled,
                                   bState3Constructed,
                                   bState3Destructed,
                                   bState3EventHandlerCalled,
                                   iLine
                                   );
   }
};

