/** @file
 ** @brief The THandleEventTypeInfo template class declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_THandleEventTypeInfoImpl_H__
#define __ILULibStateMachine_THandleEventTypeInfoImpl_H__

#include "Logging.h"
#include "CStateChangeException.h"

namespace ILULibStateMachine {
   /** Constructor.
    **/
   template <class TEventData> 
   THandleEventTypeInfo<TEventData>::THandleEventTypeInfo(
      BFTypeHandler handler,      //< Event handler to be called.
      SPCreateState spCreateState //< Describes the state transition following this handler. 
      )
      : CHandleEventInfoBase()
      , m_TypeHandler(HandlerTypeCreateState(handler, spCreateState))
   {
   }; 
   
   /** Call the handler.
    **
    ** @return a HandleResult instance indicating whether a handler was called or not and, when one was called, what the accompanying state transition is (CCreateState).
    **/
   template <class TEventData> 
   CHandleEventInfoBase::HandleResult THandleEventTypeInfo<TEventData>::Handle(
      const bool              bDefaultState, //< Indicator whether this function is called for the default state or the current state, logging only.
      SPEventBase             spEventBase,   //< Event descriptor.
      const TEventData* const pEventData     //< Data accompanying the event, will be provided to the handler.
      )
   {
      const char* const szType = bDefaultState ? "default-state" : "state";;
      
      CLogIndent logIndent;
      
      //call the type handler
      std::stringstream ss;
      ss << "Calling " << szType << " type handler\n";
      return CallHandler(
                         ss.str(),
                         TYPESEL::get<0>(m_TypeHandler), 
                         TYPESEL::get<1>(m_TypeHandler), 
                         spEventBase, 
                         pEventData, 
                         szType
                         );
   };

   /** Without further ado, call the handler and deal with exceptions.
    **
    ** @return a HandleResult instance indicating whether a handler was called or not and, when one was called, what the accompanying state transition is (CCreateState).
    **/
   template <class TEventData> 
   CHandleEventInfoBase::HandleResult THandleEventTypeInfo<TEventData>::CallHandler(
      const std::string&                                                                   strMsg,        //< Logging accompanying the handler call.
      TYPESEL::function<void(SPEventBase spEventBase, const TEventData* const pEventData)> handler,       //< The handler to be called.
      SPCreateState                                                                        spCreateState, //< The CCreateState instance accompanying the handler. Will not be called but will be included in the return value. Can be overridden if a state-change exception was caught while calling the handler.
      SPEventBase                                                                          spEventBase,   //< Event descriptor.
      const TEventData* const                                                              pEventData,    //< Data accompanying the event, will be provided to the handler.
      const char* const                                                                    szType         //< Indicator whether this function is called for the default state or the current state, logging only.
      )
   {
      try {
         LogNotice("%s", strMsg.c_str());
         {
            CLogIndent logIndent;
            handler(spEventBase, pEventData);
         }
         LogNotice("Calling type handler done\n");
         return CHandleEventInfoBase::HandleResult(true, spCreateState);
      } catch(CStateChangeException& ex) {
         LogWarning("State-change caught while calling %s type handler: %s\n", szType, ex.what());
         return CHandleEventInfoBase::HandleResult(true, ex.GetCreateState());
      } catch(std::exception& ex) {
         LogErr("Exception caught while calling %s type handler: %s\n", szType, ex.what());
         return CHandleEventInfoBase::HandleResult(true, SPCreateState());
      } catch(...) {
         LogErr("Exception caught while calling %s type handler: %s\n", szType, "unknown");
         return CHandleEventInfoBase::HandleResult(true, SPCreateState());
      }
   } 
};

#endif //__ILULibStateMachine_THandleEventTypeInfoImpl_H__

