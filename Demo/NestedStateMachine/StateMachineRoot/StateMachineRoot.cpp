/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "ToState.h"

#include "CData.h"
#include "CState1.h"
#include "StateMachineRoot.h"

using namespace ILULibStateMachine;

//StateMachineRoot common implementation
namespace DemoNestedStateMachineRoot {
   SPStateMachine CreateStateMachine(void)
   {
      //create & return state machine
      return SPStateMachine(
         new TStateMachineWithData<Internal::CData>(
            "state-machine-root",                                                //< name used for logging
            Internal::SPData(new Internal::CData("state machine root message")), //< single state machine data instance
            ToState<Internal::CState1, Internal::CData>()                        //< used to create the initial state
         )
      );
   }
};

