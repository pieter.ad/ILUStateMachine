/** @file
 ** @brief The CStateMachine declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CStateMachine__H__
#define __ILULibStateMachine_CStateMachine__H__

#include "Types.h"

#include "CCreateState.h"
#include "TEventEvtId.h"
#include "TypedefsEvent.h"

namespace ILULibStateMachine {
   /** @brief This is the state machine engine, maintaining state and handling events.
    **
    ** When a statemachine has completely finished, the m_pState can become nullptr.
    **
    ** The class will creates and own the states.
    **
    **/
   class CStateMachine {
      public:
                                                   CStateMachine(const char* szName, SPCreateState spCreateState, SPCreateState spCreateDefaultState = SPCreateState());
         virtual                                   ~CStateMachine(void);

      public:
         const std::string&                         GetName(void) const;
         bool                                       HasFinished(void) const;
         template <class TEventData, class EvtId>                                                    
         bool                                       EventHandle(
            const TEventData* const pEventData,
            const EvtId             evtId     
            );
         template <class TEventData, class EvtId, class EvtSubId1>                                                    
         bool                                       EventHandle(
            const TEventData* const pEventData,
            const EvtId             evtId     ,
            const EvtSubId1         evtSubId1  
            );
         template <class TEventData, class EvtId, class EvtSubId1, class EvtSubId2>                                                    
         bool                                       EventHandle(
            const TEventData* const pEventData,
            const EvtId             evtId     ,
            const EvtSubId1         evtSubId1 , 
            const EvtSubId2         evtSubId2  
            );
         template <class TEventData, class EvtId, class EvtSubId1, class EvtSubId2, class EvtSubId3>                                                    
         bool                                       EventHandle(
            const TEventData* const pEventData,
            const EvtId             evtId     ,
            const EvtSubId1         evtSubId1 , 
            const EvtSubId2         evtSubId2 , 
            const EvtSubId3         evtSubId3      
            );
         template <class TEventData>                                                    
         bool                                       EventHandle(
            const TEventData* const pEventData,
            const SPEventBase       spEventBase
            );

      protected:
                                                   CStateMachine(const char* szName);
         virtual CState*                           CreateStateWithData(SPCreateState spCreateState);
         CState*                                   CreateState(SPCreateState spCreateState);
         CState*                                   CreateStateCatchEx(SPCreateState spCreateState, const bool bIsDefaultState);
         void                                      ConstructStates(SPCreateState spCreateState, SPCreateState spCreateDefaultState = SPCreateState());

      private:
                                                   CStateMachine(CStateMachine& ref); //defined, not implemented --> avoid copy
         CStateMachine                             operator=(CStateMachine& ref);     //defined, not implemented --> avoid copy
         void                                      ConstructorCheck(void);
         void                                      ChangeState(SPCreateState spCreateState);
         EventMap*                                 EventGetMap(const bool bDefault);
         const EventMap*                           EventGetMap(const bool bDefault) const;
         EventTypeMap*                             EventTypeGetMap(const bool bDefault);
         const EventTypeMap*                       EventTypeGetMap(const bool bDefault) const;
         void                                      TraceAll(void) const;
         void                                      TraceHandlers(const bool bDefault) const;
         void                                      TraceTypeHandlers(const bool bDefault) const;
         std::string                               GetStateName(const bool bDefault = false) const; 
         template <class TEventData>                                                    
         bool                                      EventHandle(
            const bool              bDefault   ,
            const TEventData* const pEventData ,
            const SPEventBase       spEventBase
            );
         template <class TEventData>                                                    
         bool                                      EventTypeHandle(
            const bool              bDefault   ,
            const TEventData* const pEventData ,
            const SPEventBase       spEventBase
            );

      private:
         const std::string                         m_strName;             //< The state machine name, logging only.
         CState*                                   m_pDefaultState;       //< Pointer to the default state. Owned and deleted by the state machine when it is destructed itself. Raw pointer since fine-grained control over life-time is required (on-exit/on-entry functions).
         CState*                                   m_pState;              //< Pointer to the current state. Created and deleted by the state machine during state transitions. Raw pointer since fine-grained control over life-time is required (on-exit/on-entry functions)
   };

   /** Define a shared pointer to CStateMachine.
    **/
   typedef TYPESEL::shared_ptr<CStateMachine>      SPStateMachine;
}

#define HANDLER(edt,cl,f)         TYPESEL::function<void(const edt* const)>(TYPESEL::bind(&cl::f, this, TYPESEL_PLACEHOLDERS_1))
/** Macro eases definition of a guard and a handler upon event registration. 
 ** - First parameter: event data type; 
 ** - Second parameter: class; 
 ** - Third parameter: class guard method; 
 ** - Fourth parameter: class event method.
 **/
#define GUARD_HANDLER(edt,cl,g,f) TYPESEL::function<bool(const edt* const)>(TYPESEL::bind(&cl::g, this, TYPESEL_PLACEHOLDERS_1)),TYPESEL::function<void(const edt* const)>(TYPESEL::bind(&cl::f, this, TYPESEL_PLACEHOLDERS_1)) 
/** Macro eases definition of an event-type handler upon event registration. 
 ** - First parameter: event type;
 ** - Second parameter: event data type; 
 ** - Third parameter: class; 
 ** - Fourth parameter: class method.
 **/
#define HANDLER_TYPE(et,edt,cl,f) ILULibStateMachine::TEventEvtId<et>::IdTypeInit().c_str(),TYPESEL::function<void(ILULibStateMachine::SPEventBase, const edt* const)>(TYPESEL::bind(&cl::f, this, TYPESEL_PLACEHOLDERS_1, TYPESEL_PLACEHOLDERS_2))

//include the class template function definitions.
#include "CStateMachineImpl.h"

#endif //__ILULibStateMachine_CStateMachine__H__

