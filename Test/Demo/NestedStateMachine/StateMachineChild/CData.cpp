/** @file
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#include "Logging.h"

#include "CData.h"

using namespace ILULibStateMachine;

namespace DemoNestedStateMachineChild {
   CData::CData(const std::string& strInfo)
      : m_strInfo(strInfo)
   {
      LogInfo("Child state machine data constructed with info [%s]\n", m_strInfo.c_str());
   }
   
   //destructor only used for demo tracing
   CData::~CData(void)
   {
      LogInfo("Child state machine data destructed with info [%s]\n", m_strInfo.c_str());
   }
   
   void CData::InfoSet(const std::string& strInfo)
   {
      m_strInfo = strInfo;
   }
   
   std::string CData::InfoGet(void) const
   {
      return m_strInfo;
   }
};

