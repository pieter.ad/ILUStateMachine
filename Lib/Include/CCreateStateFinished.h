/** @file
 ** @brief The CCreateStateFinished declaration.
 **
 ** ILUStateMachine is a library implementing a generic state machine engine.
 ** Copyright (C) 2018 Ivo Luyckx
 **
 ** This library is free software; you can redistribute it and/or
 ** modify it under the terms of the GNU Lesser General Public
 ** License as published by the Free Software Foundation; either
 ** version 2.1 of the License, or (at your option) any later version.
 ** 
 ** This library is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 ** Lesser General Public License for more details.
 ** 
 ** You should have received a copy of the GNU Lesser General Public
 ** License along with this library; if not, write to the Free Software
 ** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 **
 **/
#ifndef __ILULibStateMachine_CCreateStateFinished__H__
#define __ILULibStateMachine_CCreateStateFinished__H__

#include "CCreateState.h"

namespace ILULibStateMachine {
   /** @brief The CCreateStateFinished is used to define a state transition to 'finished'.
    **
    ** 'finished' means the state machine has no 'current' state anymore.
    ** The default state still exists and is still handling events,
    ** the state machine data instance still exists.
    **/
   class CCreateStateFinished : public CCreateState {
      public:
                         CCreateStateFinished(void);

      public:
         virtual CState* CreateState(void) const;
   };

   SPCreateState GetCreateStateFinished(void);
}

#define FINISHED_STATE_CHANGE GetCreateStateFinished() ///< Define to make a state transition to the finished state easy and more clear

#endif //__ILULibStateMachine_CCreateStateFinished__H__

